/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Cliente;
import Modelo.cuentaBanco;
import Vista.dlgCuenta;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


/**
 *
 * @author _Enrique__
 */

public class Controlador implements ActionListener {
    
    private dlgCuenta Vista;
    private Cliente cliente;
    private cuentaBanco cuenta;
    
    Cliente clie = new Cliente();
    float cantidad;
    
    public Controlador(dlgCuenta Vista, cuentaBanco cuenta){
        
        this.Vista=Vista;
        this.cliente=cliente;
        this.cuenta=cuenta;
        //hacer que el controlador escuche los botones de la vista
        Vista.btnNuevo.addActionListener(this);
        Vista.btnGuardar.addActionListener(this);
        Vista.btnLimpiar.addActionListener(this);
        Vista.btnDeposito.addActionListener(this);
        Vista.btnRetiro.addActionListener(this);
        Vista.btnMostrar.addActionListener(this);
        Vista.rbtnMas.addActionListener(this);
        Vista.rbtnFem.addActionListener(this);
        Vista.btnCerrar.addActionListener(this);
        Vista.btnLimpiar.addActionListener(this);
        
    }
    
    public void btnLimpiar(){
        
        Vista.txtNumC.setText("");
        Vista.txtFecha.setText("");
        Vista.txtNombre.setText("");
        Vista.txtDomicilio.setText("");
        
        Vista.txtNomBan.setText("");
        Vista.txtPorRendi.setText("");
        Vista.txtNuSaldo.setText("");
        Vista.txtFechaAper.setText("");
        
        Vista.txtNuSaldo.setText("");
        Vista.txtCantidad.setText("");
        
        Vista.buttonGroup1.clearSelection();
    }
    

    
       private void iniciarVista(){
    
        Vista.setTitle("Productos");
        Vista.setSize(800,800);
        Vista.setVisible(true);
        
    }  
       
    
    @Override
    public void actionPerformed(ActionEvent e) {
     
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(e.getSource()==Vista.btnNuevo){
            
     
            Vista.txtNumC.setEnabled(true);
            Vista.txtFecha.setEnabled(true);
            Vista.txtNombre.setEnabled(true);
            Vista.txtDomicilio.setEnabled(true);

            Vista.txtNomBan.setEnabled(true);
            Vista.txtPorRendi.setEnabled(true);
            Vista.txtNuSaldo.setEnabled(true);
            Vista.txtFechaAper.setEnabled(true);

            Vista.txtSaldo.setEnabled(true);
            Vista.txtCantidad.setEnabled(true);
            
            Vista.rbtnFem.setEnabled(true);
            Vista.rbtnMas.setEnabled(true);
            
            Vista.btnCerrar.setEnabled(true);
            Vista.btnLimpiar.setEnabled(true);
            Vista.btnGuardar.setEnabled(true);
            Vista.btnMostrar.setEnabled(true);
            
        }
        if(e.getSource()==Vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(Vista,"¿Deseas salir?",
            "Decide", JOptionPane.YES_NO_OPTION);
             if(option==JOptionPane.YES_NO_OPTION){
                 Vista.dispose();
                 System.exit(0);
            
             }
            
        }
        if(e.getSource()==Vista.btnGuardar){
             try{
                 cuenta.setNumCuenta(Vista.txtNumC.getText());
                 cuenta.setFecha(Vista.txtFecha.getText());
                 cuenta.setNomBanco(Vista.txtNomBan.getText());
                 clie.setNom(Vista.txtNombre.getText());
                 clie.setFecha(Vista.txtFecha.getText());
                 clie.setDomi(Vista.txtDomicilio.getText());
                 cuenta.setPorcentaje(Float.parseFloat(Vista.txtPorRendi.getText()));
                 cuenta.setSaldo(Float.parseFloat(Vista.txtSaldo.getText()));
                 if(Vista.rbtnFem.isSelected()){
                    clie.setSexo(1); 
                 }
                 else if(Vista.rbtnMas.isSelected()){
                    clie.setSexo(2);
                 }
                 Vista.btnDeposito.setEnabled(true);
                 Vista.btnRetiro.setEnabled(true);
                 Vista.txtSaldo.setEnabled(false);
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(Vista, "Surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(Vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
        }
        if(e.getSource()==Vista.btnLimpiar){
            btnLimpiar();
        }
        if(e.getSource()==Vista.btnMostrar){
            Vista.txtNumC.setText(cuenta.getNumCuenta());
            Vista.txtFechaAper.setText(cuenta.getFecha());
            Vista.txtNomBan.setText(cuenta.getNomBanco());
            Vista.txtPorRendi.setText(Float.toString(cuenta.getPorcentaje()));
            Vista.txtSaldo.setText(Float.toString(cuenta.getSaldo()));
            Vista.txtNombre.setText(clie.getNom());
            Vista.txtDomicilio.setText(clie.getDomi());
            Vista.txtFecha.setText(clie.getFecha()); 
            if(clie.getSexo()==1){ 
                 Vista.rbtnFem.setSelected(true);
                
            }
            else{ 
                Vista.rbtnMas.setSelected(true); 
            }
            Vista.txtCantidad.setText(Float.toString(cantidad));
        }
        if(e.getSource()==Vista.btnDeposito){
            try{
                cantidad=Float.parseFloat(Vista.txtCantidad.getText());
                cuenta.depositar(cantidad);
                JOptionPane.showMessageDialog(Vista,"Se deposito con exito");
            
                Vista.txtNuSaldo.setText(Float.toString(cuenta.getSaldo()));
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(Vista, "Surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(Vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
        }
        if(e.getSource()==Vista.btnRetiro){
            try{
                cantidad=Float.parseFloat(Vista.txtCantidad.getText());
                if(cuenta.retirar(cantidad)==true){
                    JOptionPane.showMessageDialog(Vista,"Se retiro con exito");

                    Vista.txtNuSaldo.setText(Float.toString(cuenta.getSaldo()));
                }
                else{
                    int option=JOptionPane.showConfirmDialog(Vista,"No tiene fondo suficiente ¿Deseas volver a intentar?",
                            "Decide", JOptionPane.YES_NO_OPTION);
                    if(option==JOptionPane.YES_NO_OPTION){
                        Vista.txtCantidad.setText("");
                        Vista.txtCantidad.requestFocus(); 
                    }
                }
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(Vista, "Surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(Vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
        }  
    } 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        cuentaBanco cue = new cuentaBanco();
        dlgCuenta vista = new dlgCuenta(new JFrame(), true);
        Controlador contra = new Controlador(vista, cue);
        contra.iniciarVista();
    }
    
}
