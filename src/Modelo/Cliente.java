/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author _Enrique_
 */
public class Cliente {
    
    String nomClie,fecha,domicilio;  
    int sexo;
    
    public Cliente(){
        
        this.nomClie="";
        this.domicilio="";
        this.fecha="";
        this.sexo=0;         
    }
    
    public Cliente(Cliente clie){
        
        this.nomClie=clie.nomClie;
        this.fecha=clie.fecha;
        this.domicilio=clie.domicilio;
        this.sexo=clie.sexo;
    }
    
    public Cliente(String nom, String fecha, String domici, int sexo){
        
        this.nomClie=nom;
        this.fecha=fecha;
        this.domicilio=domici;
        this.sexo=sexo;              
    }
    
    public void setNom(String nom){
        
        this.nomClie=nom;
      
    }
    
    public String getNom(){
        
        return nomClie;
        
    }
    
    public void setFecha(String fecha){
        
        this.fecha=fecha;
        
    }
    
    public String getFecha(){
        
        return fecha;
        
    }
    
    public void setDomi(String domi){
        
        this.domicilio=domi;
        
    }
    
    public String getDomi(){
        
        return domicilio;
        
    }
    
    public void setSexo(int sexo){
        
        this.sexo=sexo;
        
    }
    
    public int getSexo(){
        
        return sexo;
        
    }
    
    
}
