/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author _Enrique_
 */
public class cuentaBanco {
    
    String numCuenta, nomBanco, fecha;
    Cliente datosClie;
    float porcentaje, saldo,newSaldo;
    
    public cuentaBanco(){
        
        this.numCuenta="";
        this.datosClie=null;
        this.fecha="";
        this.nomBanco="";
        this.porcentaje=0.02f;
        this.saldo=0.02f;
        
    }
    
    public cuentaBanco(cuentaBanco cue){
        
        this.numCuenta=cue.numCuenta;
        this.datosClie=cue.datosClie;
        this.fecha=cue.fecha;
        this.nomBanco=cue.nomBanco;
        this.porcentaje=cue.porcentaje;
        this.saldo=cue.saldo;
        
    }
    
    public cuentaBanco(String numC, Cliente datos, String fecha, String nomBan, float porce, float saldo){
        
        this.numCuenta=numC;
        this.datosClie=datos;
        this.fecha=fecha;
        this.nomBanco=nomBan;
        this.porcentaje=porce;
        this.saldo=saldo;
        
    }
    
    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNomBanco() {
        return nomBanco;
    }

    public void setNomBanco(String nomBanco) {
        this.nomBanco = nomBanco;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Cliente getDatosClie() {
        return datosClie;
    }

    public void setDatosClie(Cliente datosClie) {
        this.datosClie = datosClie;
    }

    public float getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(float porcentaje) {
        this.porcentaje = porcentaje;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    public float getNewSaldo() {
        return newSaldo;
    }

    public void setNewSaldo(float Newsaldo) {
        this.newSaldo = Newsaldo;
    }
    
    public void depositar(float depo){
        this.saldo+=depo;
        
    }
    
    public boolean retirar(float reti){
        if(reti<=this.saldo){
            saldo=saldo-reti;
            return true;
        }
        else{
            return false;
        }
        
    }
    
    public float calcularReti(){
        
        return 0;
    }
    
}
